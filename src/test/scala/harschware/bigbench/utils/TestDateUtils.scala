package harschware.bigbench.utils

import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.junit._
import org.junit.Assert._
import org.junit.Test
import org.slf4j.LoggerFactory
import com.github.nscala_time.time.Imports._

@Test
class TestDateUtils {
  var LOGGER = LoggerFactory
    .getLogger(classOf[TestDateUtils]);

  val testFileUrl = Thread.currentThread()
    .getContextClassLoader().getResource("chartList.txt")

  @Test
  def testJustTime() = {
    val dtfIso = ISODateTimeFormat.hourMinuteSecond()

    val expectedSecs = dtfIso.parseDateTime("00:00:12")
    LOGGER.debug(dtfIso.print(expectedSecs))

    val actualSecs = DateUtils.parseDate("12s", DateUtils.FORMATTERS_JUSTTIME);
    LOGGER.debug(dtfIso.print(actualSecs))

    Assert.assertEquals(expectedSecs, actualSecs);
  } // end test

  @Test
  def testPeriod() = {
    val acutalPeriod = DateUtils.parsePeriod("1h5m12s")
    LOGGER.debug(DateUtils.printPeriod(acutalPeriod))

    val expectedPeriod = (1.hours + 5.minutes + 12.seconds).toPeriod
    Assert.assertEquals(expectedPeriod, acutalPeriod);

    val acutalPeriod2 = DateUtils.parsePeriod("1h:09m:44s:440ms")
    LOGGER.debug(DateUtils.printPeriod(acutalPeriod2))

    val expectedPeriod2 = (1.hours + 9.minutes + 44.seconds + 440.millis).toPeriod
    Assert.assertEquals(expectedPeriod2, acutalPeriod2);
  } // end test

  @Test
  def testDates() = {
    var expectedDate = new DateTime(2014, 12, 7, 23, 25, 0, DateTimeZone.forID("US/Pacific"));
    LOGGER.debug(expectedDate.toString)

    // e.g.  yyyy-MM-dd'THH:mm:ss.SSS-offset  e.g.  2014-12-07T23:25:00.000-08:00
    var actualDate = DateUtils.parseDate("2014-12-07T23:25:00.000-08:00") // format 1
    LOGGER.debug(s"NOTE: Chronlogy of constructed dates and parsed dates are not the same, if parsing offsets: expected=${expectedDate}, actual=${actualDate}")
    LOGGER.debug(actualDate.toString)
    Assert.assertTrue(!expectedDate.getChronology().equals(actualDate.getChronology()));
    Assert.assertEquals(expectedDate.getMillis(), actualDate.getMillis());

    // reset the zone to the current zone for remaining tests (or tests could fail
    //  on differing time zones, since parseDate assumes current time zone if not specified)
    expectedDate = new DateTime(2014, 12, 7, 23, 25, 0)

    // e.g. "MM/dd/yyyy HH:mm" "12/07/2014 19:25"
    actualDate = DateUtils.parseDate("12/07/2014 23:25") // format 2
    LOGGER.debug(actualDate.toString())
    Assert.assertEquals(expectedDate, actualDate);

    // e.g. "MMM dd, yyyy HH:mm:ss a" "Dec 07, 2014 11:25:36PM"
    actualDate = DateUtils.parseDate("Dec 07, 2014 11:25:00 AM") // format 3
    LOGGER.debug(actualDate.toString())
    Assert.assertEquals(expectedDate.minusHours(12), actualDate);

    actualDate = DateUtils.parseDate("Dec 07, 2014 11:25:00 PM") // format 3
    LOGGER.debug(actualDate.toString())
    Assert.assertEquals(expectedDate, actualDate);

    // e.g. yyyy-MM-dd'T'HH:mm     2014-12-06T23:25
    actualDate = DateUtils.parseDate("2014-12-07T23:25") // format 4
    LOGGER.debug(actualDate.toString())
    Assert.assertEquals(expectedDate, actualDate);
  } // end test
} // end test class