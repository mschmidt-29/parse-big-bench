package harschware.bigbench.utils

import java.io.File
import scala.collection.mutable.MutableList
import scala.io.Source
import org.slf4j.LoggerFactory

object BigBenchLogUtils {
    var LOGGER = LoggerFactory
    .getLogger(BigBenchLogUtils.getClass());
    
  def getQueryLogs(logPath: String): Stream[File] = {
    val ft = FileUtils.getFileTree(new File(logPath))
    ft.filter(_.getName.matches("""hiveLoading.log|q[0-9][0-9].*\.log"""))
  }

  def getErrors(logPath: String) = {
    val grepVals = FileUtils.grep(getQueryLogs(logPath), """.*(FAIL|ERROR\:|Could not|Exception|unexpected).*""")
    grepVals.filter(x => !x._2.matches(".*Failed Shuffles=0.*"))
    grepVals.filter(x => !x._2.matches(".*table not found.*")) // spark throws exceptions when executing 'IF EXISTS' query parts
  }

  def testNameToShortLabel(name: String, stream: String, query: String, engine: String): String = {
	
    var formattedEngine = engine
    formattedEngine = formattedEngine.capitalize
    var formattedName = name
    formattedName = "_IN_PROGRESS".r.replaceAllIn(formattedName, "")
    formattedName = """TEST_FIRST_QUERY_RUN""".r.replaceAllIn(formattedName, "1st Run")
    formattedName = """TEST_SECOND_QUERY_RUN""".r.replaceAllIn(formattedName, "2nd Run")
    formattedName = formattedName.toLowerCase.split("_").map(_.capitalize) mkString " "
    var label = formattedName + " S" + stream + " Q" + query + " " + formattedEngine;
	
	
	
	return label
  }

  def getJobReports(logPath: File): Map[String, List[AnyVal]] = {
    	
	val ft = FileUtils.getFileTree(logPath)
    val logfiles = ft.filter(_.getName.matches("""q[0-9][0-9].*\.log"""))

    var jobSummary: scala.collection.mutable.Map[String, List[AnyVal] ] = scala.collection.mutable.Map()
    
    for (logfile <- logfiles) {
      val (real, jobreport) = getJobStats(logfile)

      // Now sum and average everything
      var (jobs, mappers, reducers, cpu, reads, writes) = (0, 0, 0, 0, 0L, 0L);
      for (row <- jobreport) {
        jobs += 1;
        mappers += row(1).asInstanceOf[Number].intValue();
        reducers += row(2).asInstanceOf[Number].intValue();
        cpu += row(3).asInstanceOf[Number].intValue();
        reads += row(4).asInstanceOf[Number].longValue();
        writes += row(5).asInstanceOf[Number].longValue();
		
      } // end for
      
      jobSummary(logfile.getName()) = List(real, jobs, mappers, reducers, cpu, reads, writes)
    } // end for
	
		
	
    return jobSummary.toMap
  } // end test


  def getJobStats(logFile: File): (Int, List[List[AnyVal]]) = {

    var lines = FileUtils.fileLines(logFile)
    //var logContent = Source.fromFile(logFile).mkString
    
    var jobreport: MutableList[List[AnyVal]] = MutableList()
    val jobStat = """([^:]+):.*?Map: (\d+)\s+(?:Reduce:\s+)?(\d+)?\s+Cumulative CPU:\s+([0-9.]+).+?HDFS Read:\s+(\d+)\s+HDFS Write:\s+(\d+)""".r
    //val realtime = """real\s+(\S+)""".r
	val realtime = """Duration:\s+(.*)""".r 
	
    var real = ""
	var result = ""
	for (line <- lines) {
	   
      LOGGER.debug(line)
      for (jobStat(j, maps, r, cpu, reads, writes) <- jobStat.findAllIn(line)) {      
        var reducer = if( r==null) 0 else r.toInt
        jobreport += List(j, maps.toInt, reducer, cpu.toFloat.toInt, reads.toLong, writes.toLong)
		
      } // end for
	  	  
	  line match {
        case realtime(r) => real = r
        case _ => ""
      }
	  	  
	  
	} // end for
	
    val timeInSecs = timeParse(real).toFloat.toInt
    return (timeInSecs, jobreport.toList);
  } // end function

  def timeParse(real: String): Float = {

    val p1 = """([0-9.]+)s$""".r;
    val p2 = """(\d+)m\s+([0-9.]+)s$""".r;
    val p3 = """(\d+)h\s+(\d+)m\s+([0-9.]+)s$""".r;

    var hr = 0
    var min = 0
    var sec = 0.0f
    real.trim match {
      case p3(h, m, s) =>
        hr = h.toInt; min = m.toInt; sec = s.toFloat;
      case p2(m, s) =>
        min = m.toInt; sec = s.toFloat;
      case p1(s) =>
        sec = s.toFloat;
      case x => ""
    } // end match

    return hr * 60 * 60 + min * 60 + sec;
  } // end function
}