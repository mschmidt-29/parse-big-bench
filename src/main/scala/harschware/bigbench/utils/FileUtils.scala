package harschware.bigbench.utils

import java.io.File
import scala.collection.immutable.Stream.consWrapper
import java.io.PrintWriter

object FileUtils {
  /**
   * @see: http://stackoverflow.com/a/7264833/154461
   *
   * e.g. getFileTree(new File("/tmp") ).filter(_.getName.endsWith(".scala")).foreach(println)
   */
  def getFileTree(f: File): Stream[File] = {
    f #:: (if (f.isDirectory) f.listFiles().toStream.flatMap(getFileTree)
    else Stream.empty)
  } // end function

  /**
   * @see: http://booksites.artima.com/programming_in_scala_2ed/examples/html/ch07.html
   */
  def fileLines(file: java.io.File) =
    scala.io.Source.fromFile(file).getLines().toList

  /**
   * @see: http://booksites.artima.com/programming_in_scala_2ed/examples/html/ch07.html
   */
  def grep(files: Stream[File], pattern: String): Stream[(File, String)] =
    for (
      file <- files;
      line <- fileLines(file) if line.trim.matches(pattern)
    ) yield (file, line)

  def outputListToTsvFile(file: File, list: List[List[Any]]): Unit = {
    val path = file.getAbsolutePath();
    val pw = new PrintWriter(path)
    for (el <- list) {
      pw.println(el.mkString("\t"))
    } // end for
    pw.close()
  } // end funtion

} // end object