# README #

## Description
A scala based utility for parsing logs from [BigBench](https://github.com/intel-hadoop/Big-Bench) into a chartable format.  The following is a list of benefits of parse-big-bench:

  * includes only the results of individual queries (no aggregate times)  TODO:  Add aggregates as optional output
  * adjusts the labels to be shorter and title cased
  * Converts times to seconds where necessary
  * Collects way more metrics not currently in BigBenchTimes.csv file that is output by BigBench project
  * produces TSV output


## Example Output

    Test Name   time(s) jobs    mappers reducers    cumulative cpu(s)   reads (bytes)   writes (byes)
    Power Test S0 Q25       374     3       1784    564     19316   20163433101     9207847894
    Throughput 1st run S0 Q23       1134    16      492     59      11002   34144171708     12998378822 
    Throughput 1st run S0 Q18       482     5       876     221     25033   15492572527     11130943500
    Power Test S0 Q08       495     7       3543    437     41944   87343000301     8794121368

# Notes about the output
All metrics are on a per test basis, and each test in BigBench is one a cumulation of one or more map/reduce jobs.  The "jobs" column represents the number of jobs in each test.


## Usage

    % mvn clean package
    % cd target
    % java -jar parse-big-bench-1.0-SNAPSHOT.jar --logPath ~/Big-Bench/logs --queryTimes ~/Big-Bench/logs/queryTimes.tsv
    % cat queryTimes.tsv

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)